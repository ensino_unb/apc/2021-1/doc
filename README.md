# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
1º Semestre de 2.021

## Turmas
* AA - Fabricio Ataides Braz
* BB - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de **aprendizado baseado em projeto**.

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da instrução, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a investigação, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da instrução, para a tutoria, no que diz respeito ao ensino. A perspectiva do aluno muda de passiva para ativa, no que diz respeito ao aprendizado.

Mesmo que nosso foco seja o projeto, observamos a necessidade de estimular a turma a adquirir habilidades fundamentais para o desenvolvimento de algoritmos. Em razão disso, decidimos lançar mão da instrução no início do período letivo. Momento em que passaremos pela sensibilização sobre lógica de programação, usando para isso os recursos de [programação em blocos](https://code.org), e depois fundamentos de [programação em Python](https://github.com/PenseAllen/PensePython2e/).

A disciplina prevê um total de 90 horas de formação, a serem distribuídas da seguinte maneira:

| Horas | Atividade                          | Natureza |
|-------|------------------------------|---------|
| 18   | Acolhimento e nivelamento              | Mista |
| 21   | Tutorias                                | Síncrona |
| 38   | Atividades em Grupo                    | Assíncrona |
| 1    | Planejamento                           | Assíncrona |
| 12    | Seminários                           | Síncrona |

Após o momento de instrução mencionado anteriormente, serão constituídos grupos em que o aluno, apoioado por até 20 (vinte) tutores (professores e monitores), percorrerá uma trilha de aprendizagem voltada para a construção de dashboards.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/team/19%3ab29fd5b216894a86a8033efae42bcb7b%40thread.tacv2/conversations?groupId=d59cb7be-a0ea-458d-94ea-eeafccc5e38b&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo
* [Trello](https://trello.com/b/9abnJY4R/project-based-learning-apc) - gerência do projeto
* [Python](https://www.python.org/) - Linguagem de programação
* [Plotly](https://plotly.com/) - Biblioteca de construção de dashboard
* [Gitlab](https://gitlab.com/ensino_unb/apc/2021_1/doc) - Reposotório de código e coloboração
* [Code](https://studio.code.org/home#classroom-sections) - Programação em blocos
    * Chave - ******
* [URI](https://www.urionlinejudge.com.br/judge/en/disciplines/join) - Corretor automático de solução
    * Id da disciplina - ****
    * Chave - *****


## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* ExCODE: nota correspondente ao percentual de exercícios resolvidos na plataforma code.org

* ExURI: nota correspondente ao percentual de exercícios resolvidos na plataforma uri.org

* AGP: avaliação do grupo pelos professores. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/gif.latex?%5Csqrt%5B2%5D%7B%5Cfrac%7B%5Csqrt%5B4%5D%7BExCode%2AExURI_1%2AExURI_2%2AExURI_3%7D%7D%7B100%7D%2A%280.45%2A%20%5Csqrt%7B%7BAGP_1%2AAIG_1%7D%7D%2B%200.55%2A%20%5Csqrt%7B%7BAGP_2%2AAIG_2%7D%7D%29%7D)

Serão dois encontros avaliativos, observando as fases de desenvolvimento, com os seguintes pesos:
1. Desenvolvimento (peso 0,45)
3. Desenvolvimento (peso 0,55)

### Cronograma
|Aula | Data | Programação | Natureza |
| --- | --- | --- | --- |
|1|19/7| Acolhida |  Síncrona  |
|2|20/7| Nivelamento sobre ferramentas de apoio |  Síncrona  |
|3|22/7| Code.org | Assíncrona|
|4|26/7| Code.org | Assíncrona|
|5|27/7| Code.org | Assíncrona|
|6|29/7| Code.org | Assíncrona|
|7|2/8|  Data limite para envio da lista code.org <br/> [Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md) |  Síncrona|
|8|3/8| [Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md) |  Síncrona  |
|9|5/8| [Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md) |  Síncrona  |
|10|9/8| [Instalação do Python e interface](https://github.com/PenseAllen/PensePython2e/blob/master/docs/04-caso-interface.md) | Síncrona |
|11|10/8| URI.org |Assíncrona|
|12|12/8| Data limite de envio da lista URI estruturas sequenciais <br/> [Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)  |  Síncrona  |
|13|16/8| URI.org |Assíncrona|
|14|17/8|  Data limite de envio da lista URI estruturas de decisão <br/> [Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md) |  Síncrona  |[Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md) |  Síncrona  |
|15|19/8| [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md) |  Síncrona  |Síncrona  |
|16|23/8| [Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md) | Síncrona | 
|17|24/8| [Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md) |  Síncrona  |
|18|26/8|  URI.org |Assíncrona|
|19|30/8|  Plotly <br/> Brainstorm & Research <br/> Data limite de envio da lista URI estrutura de repetição |  Assíncrona  |
|20|31/8| Plotly <br/> Brainstorm & Research |  Assíncrona  |
|21|2/9| Plotly <br/> Design |  Assíncrona  |
|22|6/9| Plotly <br/> Design |  Assíncrona  |
||7/9| Feriado| |
|23|9/9| Plotly <br/> Design |  Assíncrona  |
|24|13/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|25|14/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|26|16/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|27|20/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|28|21/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|29|23/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|30|27/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|31|28/9| Plotly <br/> Desenvolvimento |  Assíncrona  |
|32|30/9| Dash <br/> Desenvolvimento |  Assíncrona  |
|33|4/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|34|5/10| Avaliação 1 |  Síncrona  |
|35|7/10| Avaliação 1 |  Síncrona  |
|36|11/10| Avaliação 1 |  Síncrona  |
||12/10| Feriado| |
|37|14/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|38|18/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|39|19/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|40|21/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|41|25/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|42|26/10| Dash <br/> Desenvolvimento |  Assíncrona  |
|43|28/10| Avaliação 2 |  Síncrona  |
|44|1/11| Avaliação 2 |  Síncrona  |
||2/11| Feriado |    |
|45|4/11| Avaliação 2 |  Síncrona  |

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade de até 9 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de membros do seu grupo, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para apresentação.

### Evasão
Grupos com menos de 5 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers
